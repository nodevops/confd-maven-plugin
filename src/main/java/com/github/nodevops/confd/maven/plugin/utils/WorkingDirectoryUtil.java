package com.github.nodevops.confd.maven.plugin.utils;

import static com.github.nodevops.confd.maven.plugin.ConfdConsts.CONF_D_DIRECTORY;
import static com.github.nodevops.confd.maven.plugin.ConfdConsts.TEMPLATES_DIRECTORY;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import org.codehaus.plexus.util.FileUtils;

import com.github.nodevops.confd.maven.plugin.model.TemplateConfig;

public class WorkingDirectoryUtil {
    private static final char QUOTE = '\'';
    private static final char LINE_SEPARATOR = '\n';

    public static void generateConfdArtefacts(
        File workingDirectory,
        List<TemplateConfig> templates,
        boolean forceDestToLocalFileSystemType) throws IOException {

        File templatesDirectory = new File(workingDirectory, TEMPLATES_DIRECTORY);
        File tomlDirectory = new File(workingDirectory, CONF_D_DIRECTORY);

        if (workingDirectory.exists()) {
            FileUtils.deleteDirectory(workingDirectory);
        }

        FileUtils.mkdir(templatesDirectory.getAbsolutePath());
        FileUtils.mkdir(tomlDirectory.getAbsolutePath());
        for (TemplateConfig tc : templates) {
            String tomlBaseName = FileUtils.basename(tc.getSrc().getAbsolutePath()) + "toml";
            File tomlFile = new File(tomlDirectory, tomlBaseName);
            writeToml(tomlFile, tc, forceDestToLocalFileSystemType);
            FileUtils.copyFileToDirectory(tc.getSrc(), templatesDirectory);
        }
    }

    public static void writeToml(
        File tomlFile,
        TemplateConfig tc,
        boolean globalForceDestToLocalFileSystemType) throws IOException {
        StringWriter sw = new StringWriter();
        sw.append("[template]")
            .append(LINE_SEPARATOR);

        sw.append("src = ")
            .append(QUOTE)
            .append(FileUtils.removePath(tc.getSrc().getPath()))
            .append(QUOTE)
            .append(LINE_SEPARATOR);

        sw.append("dest = ")
            .append(QUOTE)
            .append(tc.getResolvedDestPath(globalForceDestToLocalFileSystemType))
            .append(QUOTE)
            .append(LINE_SEPARATOR);

        sw.append("keys = [")
            .append(LINE_SEPARATOR);
        for (String key : tc.getKeys()) {
            sw.append(QUOTE)
                .append(key)
                .append(QUOTE)
                .append(',')
                .append(LINE_SEPARATOR);
        }

        sw.append(']')
            .append(LINE_SEPARATOR);

        FileUtils.fileWrite(tomlFile, "UTF8", sw.toString());
    }

}
