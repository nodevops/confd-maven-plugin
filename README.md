Linux build: [![Build Status](https://travis-ci.org/nodevops/confd-maven-plugin.svg?branch=master)](https://travis-ci.org/nodevops/confd-maven-plugin)

[![codecov](https://codecov.io/gh/nodevops/confd-maven-plugin/branch/master/graph/badge.svg)](https://codecov.io/gh/nodevops/confd-maven-plugin)


# confd-maven-plugin

# Contribute

see [coding-rules-and-conventions.adoc](https://github.com/nodevops/confd-maven-plugin/tree/master/src/site/asciidoc/coding-rules-and-conventions.adoc)

# Licence

```
Copyright 2015 NoDevOps

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
